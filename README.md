See blog post here: https://medium.com/@kormat/managing-iptables-using-ansible-v2-fc2034d5bcd9

## Iptables-Apply

This repo contains an ansible role + script for remotely managing iptables
using ansible. It requires ansible >= 2.6.8, and expects your iptables rules to
be in the `iptables-restore` format.

## Role parameters

| name                    | value    | optionnal | default value   | description                              |
| ------------------------|----------|-----------|-----------------|------------------------------------------|
| rules_v4_file           | str      | no        | ''              | iptables-restore v4 rules file location  |
| rules_v6_file           | str      | no        | ''              | iptables-restore v6 rules file location  |

## Using this role

### ansible galaxy

Add the following in your requirements.yml.

```yml
---
- src: https://gitlab.com/myelefant1/ansible-roles3/iptables-apply
  scm: git
```

### Sample playbook

```yml
- hosts: all
  roles:
  - role: iptables-apply
    rules_v4_file: templates/iptables/rules.v4.tmpl
    rules_v6_file: templates/iptables/rules.v6.tmpl
```

## Tests

Writing...
